﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;



//importamos
using DbTikets.Service;
using DbTikets.Domain;

namespace DbTikects.Web.Controllers
{
    public class ProductoController : Controller
    {
        private IProductoService _service;

        public ProductoController()
        {
            if (_service == null)
            {
                _service = new ProductoService();
            }
        }
        
        // GET: Producto
        public ActionResult Index()
        {
            var lista = _service.GetProductos("");

            return View(lista);
        }

        [HttpPost]
        public ActionResult Index(string criterio)
        {
            var lista = _service.GetProductos(criterio);

            return View(lista);
        }

        public ActionResult Create()
        {
            //Tarea
            //Colocar el combo con las categorias
            //Editar y Eliminar
            return View();
        }

        [HttpPost]
        public ActionResult Create(Producto producto)
        {

           // _context.Productos.Add(producto);
            //_context.SaveChanges();
            if (ModelState.IsValid)
            {
                _service.AddProducto(producto);

                return RedirectToAction("Index");
            }
            return View();
        }
    }
}