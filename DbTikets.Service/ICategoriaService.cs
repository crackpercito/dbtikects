﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DbTikets.Domain;

namespace DbTikets.Service
{
    interface ICategoriaService
    {
        IEnumerable<Categoria> GetCategorias();
    }
}
